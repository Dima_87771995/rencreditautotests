# Проект по автоматизации тестирования для Ренессанс кредит банка
## <a target="_blank" href="https://rencredit.ru/">Веб сайт Ренкредит банка</a>

![This is an image](images/logo/Ренкредит.png)

## :rocket: Технологии и инструменты
<p align="center">
<img width="6%" title="IntelliJ IDEA" src="images/logo/Intelij_IDEA.svg">
<img width="6%" title="Selenide" src="images/logo/Selenide.svg">
<img width="6%" title="Selenoid" src="images/logo/Selenoid.svg">
<img width="6%" title="Allure Report" src="images/logo/Allure_Report.svg">
<img width="6%" title="TestNG" src="images/logo/TestNG.svg">
<img width="9%" title="GitLab" src="images/logo/Gitlab-Logo.svg">
<img width="6%" title="Jenkins" src="images/logo/Jenkins.svg">
<img width="6%" title="Telegram" src="images/logo/Telegram.svg">
</p>

## :rocket: Реализованные проверки это смоук тесты
![This is an image](https://i.imgur.com/Dd62Cq5.png)
- Наличие основных элементов раздела
## :rocket: Сборка в Jenkins
<p align="center">
<img title="Jenkins Dashboard" src="images/screenshots/jenkins_dashboard.png">
</p>

### Параметры сборки в Jenkins:
Сборка в Jenkins

- browser (браузер, по умолчанию chrome)
- version (версия браузера, по умолчанию 100.0)
- size (размер окна браузера, по умолчанию 1920x1080)
- threads (количество потоков)

## :rocket: Запуск из терминала
Локальный запуск:
```
mvn clean test -DthreadCount=$THREAD_COUNT -Dgroups=$TEST_GROUP
```

Удаленный запуск:
```
clean
test
-Dbrowser=${BROWSER}
-DbrowserSize=${BROWSER_SIZE}
-Dgroups=${TEST_GROUP}
-Dthreads=${THREADS}
```

## :rocket: Allure отчет
- ### Главный экран отчета
<p align="center">
<img title="Allure Overview Dashboard" src="images/screenshots/report_main_page.png">
</p>

- ### Страница с проведенными тестами
<p align="center">
<img title="Allure Test Page" src="images/screenshots/report_tests_page.png">
</p>

## :rocket: Отчет в Telegram
<p align="center">
<img title="Telegram notification message" src="images/screenshots/telegram_report.png">
</p>

