package config;

import org.aeonbits.owner.Config;

@Config.Sources("file:src/main/resources/test.properties")
public interface PropertiesConfig extends Config {

    @Config.Key("qa.url")
    String qa_url();

}
