package steps;

import config.PropertiesConfig;
import io.qameta.allure.Step;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.JavascriptExecutor;

import java.util.Iterator;
import java.util.logging.Logger;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverConditions.currentFrameUrl;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class BaseSteps {

    PropertiesConfig config = ConfigFactory.create(PropertiesConfig.class);
    private static final Logger LOGGER = Logger.getLogger(BaseSteps.class.getName());

    @Step("Открытие страницы")
    private void openPage(String url) {
        clearBrowserCookies();
        try {
            open(url);
        } catch (org.openqa.selenium.TimeoutException e) {
            LOGGER.warning("Browser failed to open!");
        }
    }

    @Step("Открытие стартовой страницы")
    public void openStartPage() {
        openPage(config.qa_url());
    }

    @Step("Открытие новой вкладки")
    public void openNewTab() {
        ((JavascriptExecutor) getWebDriver()).executeScript("window.open('about:blank','_blank');");
        selectTab(1);
        sleep(1000);
    }

    @Step("Выбор следующей вкладки браузера")
    public void selectTab() {
        String handle = getWebDriver().getWindowHandles().iterator().next();
        getWebDriver().switchTo().window(handle);
    }

    @Step("Выбор вкладки браузера №{0}")
    public void selectTab(int number) {
        switchTo().window(number);
    }

    @Step("Закрытие последней вкладки")
    public void closeLastTab() {
        Iterator<String> handles = getWebDriver().getWindowHandles().iterator();
        String handle = handles.next();
        while (handles.hasNext()) {
            handle = handles.next();
        }
        getWebDriver().switchTo().window(handle);
        getWebDriver().close();
    }

    @Step("Нажатие кнопки 'Назад' в браузере")
    public void pressBackButton() {
        back();
    }

    @Step("Обновить страницу в браузере")
    public void refreshPage() {
        refresh();
    }

    @Step("Проверить URL страницы")
    public BaseSteps checkPageUrl(final String url) {
        webdriver().shouldHave(currentFrameUrl(url));
        return this;
    }
}
